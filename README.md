# DinoNet

A simple website made as an academic project for the Web Technologies course of the bachelor's degree in Computer Science of the University of Padua (Italy).

## Description

The website is highly focused on accessibility, usability and syntactical correctness. It's made up both of a pure HTML5/CSS front-end (with restricted use of JS, only employed for form validation) and a PHP back-end.  

It is a simple educational blog on dinosaurs, targeting mostly children.   

## Authors

* [Matteo Rizzo](https://github.com/MatteoRizzo96)
* [Cristiano Tessarolo](https://github.com/cristianotessarolo)
* [Alessandro Zangari](https://github.com/ALIENK9)

## License

This project is licensed under the [MIT] License - see the LICENSE.md file for details
